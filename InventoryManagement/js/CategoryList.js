function CategoryList() {
	this.data;
	this.source = 'InventoryManagement/php/categories_json.php';
	this.categoryImages = {
		"Drinks": "lib/images/categories/1425731712_app_type_coffee_shop_512px_GREY.png", 
		"Snacks": "lib/images/categories/snack.jpg"
	};
	this.template = $("#categorytemplate");
	this.container = $("#categorySection");
	this.init();
	this.clickHandler;
}
CategoryList.prototype = {
	constructor: CategoryList,
	
	init: function() {
		this.load();
	},

	load: function() {
		var that = this;
		$.ajax({
			url: that.source,
			type: 'POST'
		})
		.done(function(json) {
			json = $.parseJSON(json);
			console.log(json);
			that.display(json);
		});
	},

	display: function(data) {
		var that = this;
		//console.log(data.length);
		for(var i = 0, keyLen = data.length; i < keyLen; i++) {
			var catData = data[i];

			// Create a jquery instance of template (originally string)
			var jqcat = $(that.template.html() );

			// ** Bind event before appending to container **
			that.bindCategoryClick(jqcat);

			// Start changing content of template
			jqcat.find('.categoryImg').prop('src', that.categoryImages[catData['itemType']]);
			that.container.append(jqcat);

		}
	},

	setClickHandler: function(callback) {
		this.clickHandler = function() {
			callback();	// function() { catItems.load(cat) };
		};
	},

	bindCategoryClick: function(thumbnail) {
		var that = this;
		var itemType;
		thumbnail.on('click', function() {
			itemType = thumbnail.attr('itemType');
			that.clickHandler(itemType);
		});
	}
}
