<?php
	require_once '../../lib/php/sqlConnection.php';
	include '../../lib/php/InventoryManager.php';

	$im = new InventoryManager();

	$category = $_POST['cat'];
	$r = $im->getCategoryItems($category);

	echo json_encode($r);
?>