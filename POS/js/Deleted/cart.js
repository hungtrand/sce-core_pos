function cart() {
	this.cartItems = {};
	this.numberItems = 0;
	this.cartItemsQty = {};
	this.ui = {};

	this.init();
}

cart.prototype = {
	constructor: cart,

	init: function() {
		this.ui = new uiCart(this);
	},

	addToCart: function(objItem) {
		if (typeof(this.cartItems[objItem.ID]) == 'undefined') {
			this.cartItems[objItem.ID] = objItem;
		}

		// Increment number of items in cart
		if (typeof(this.cartItemsQty[objItem.ID]) == 'undefined') {
			this.cartItemsQty[objItem.ID] = 0;
		}

		this.cartItemsQty[objItem.ID]++;
	},

	substractFromCart: function(itemID) {
		if (typeof(this.cartItemsQty[itemID]) == 'number') {
			this.cartItemsQty[itemID]--;

			if (this.cartItemsQty[itemID] <= 0) {
				this.removeFromCart(itemID);
			}
		}
	},

	removeFromCart: function(itemID) {
		if (typeof(this.cartItemsQty[itemID]) == 'number') {
			delete this.cartItemsQty[itemID];
			delete this.cartItems[itemID];
		}
	},

	updateItemQty: function(itemID, newQty) {
		if (newQty == 0) {
			this.removeFromCart(itemID);
		} else {
			this.cartItemsQty[itemID] = newQty;
		}
	},

	resetCart: function() {
		this.cartItemsQty = {};

		for (id in this.cartItems) {
			this.cartItems[id].ui.refresh();
		}

		this.cartItems = {};
	}
}