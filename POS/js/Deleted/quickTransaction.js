function quickSale(element)
{
	this.element = $(element);
	this.total = 0.00;
	this.paid = 0.00;
	this.due = 0.00;

	this.eTotal;
	this.ePaid;
	this.eDue;

	this.init();
}

quickSale.prototype = {
	constructor: quickSale,

	init: function() {
		this.bindUIEvent();
	},

	bindUIEvent: function() {
		var obj = this;

		this.element.droppable({
			hoverClass: "quickSaleHover"
		});
	},

	calculate: function (item) {
		// Calculate total
		var newTotal = item.itemPrice;
		
		// get amount paid and due
		this.total = parseFloat(newTotal).toFixed(2);
		this.paid = parseFloat(this.ePaid.val()).toFixed(2);
		this.due = this.total - this.paid;

		// update interface
		this.eTotal.val(this.total);
		this.ePaid.val(this.paid);
		this.eDue.val(this.due);
	},
}