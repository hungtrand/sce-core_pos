<?php header('Content-Type: application/json');
include "../../library/php/mySqlConnection.php";

ini_set("display_error", "on");
error_reporting(E_ALL | E_STRICT);

 session_start();
if (!isset($_SESSION['_ADMIN_'])) { 
	$OfficerID = 0;
    session_write_close();
}

$OfficerID = $_SESSION['_ADMIN_']['AdminID'];

$cn = new sqlConnection;
$db = $cn->connect("POS");

$json = [];

// Inputs
$data = json_decode(file_get_contents("php://input"), true);
$SJSUID = $data['SJSUID'];
$Sales = $data['sales'];

// Compose sql for Transactions Table
$sql1 = "INSERT INTO transactions 
		(numberOfItems, total, postedBy, SJSUID) VALUES (?, ?, ?, ?)";

// Compose sql for Sales table
$sql2 = "INSERT INTO sales 
		(itemID, itemName, itemDescription, itemPrice, quantity, idTrans) 
		VALUES (?, ?, ?, ?, ?, ?)";

$transQty = 0;
$total = 0.0;

// $i is key and it's transaction id, value as $sale each sale has quantity and price
foreach ( $Sales as $i=>$sale ) {
	$transQty += intval($sale['Quantity']);
	$total += $sale['Price'] * $sale["Quantity"];
}

// Execute first query
$idTrans = 0;

if ($stmt = $db->prepare($sql1)) 
{
	$stmt->bind_param("idis", $transQty, $total, $OfficerID, $SJSUID);

	$stmt->execute();

	if (mysqli_affected_rows($db) > 0) {
		$idTrans = $db->insert_id;
		$json["idTrans"] = $idTrans;
		$json["totalTrans"] =$total;
	} else {
		$idTrans =  0;
	}

	$stmt->close();
}

if ($idTrans != 0) {
	foreach ( $Sales as $i=>$sale ) {
		if ($stmt = $db->prepare($sql2)) 
		{
			$itemID = intval($sale['ID']);
			$itemName = $sale['Name'];
			$itemDescription = $sale['Description'];
			$itemPrice = floatval($sale['Price']);
			$itemQuantity = intval($sale['Quantity']);

			//echo ($itemID . "-" . $itemName . "-" . $itemDescription . "-" . $itemPrice . "-" . $itemQuantity . "<br />");
			$stmt->bind_param("issdii", $itemID, $itemName, $itemDescription, $itemPrice, $itemQuantity, $idTrans);

			$stmt->execute();

			if (mysqli_affected_rows($db) > 0) {
				$json["result"] = "success";
			} else {
				$json["result"] = "Failed to insert sales.";
			}

			$stmt->close();
		}
	}
}
echo json_encode($json);
session_write_close();