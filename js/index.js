(function() { 
	var location = {
		"#InventoryManagement": "InventoryManagement/index.php",


	};

	var scripts = {
		"#InventoryManagement": ['InventoryManagement/js/CategoryList.js', 'InventoryManagement/js/InventoryManagement.js']
	};

	var main = $("#main");
	var hash = window.location.hash;
	//console.log(hash);
	if(hash && location.hasOwnProperty(hash)) {	//User lands on page
		loadModule(location[hash]);
	}

	$(window).on('hashchange', function() {	
		hash = window.location.hash;

		loadModule(location[hash]);
	});

	$('.navBtn').on('click', function() {
		if(hash != $(this).attr('href') ) return;

		loadModule(location[hash]);
	});

	function loadModule(url) {
		$.ajax({
			url: url,
			type: 'POST'
		})
		.done(function(htmlContent) {
			main.html(htmlContent);
			for(var i=0, array=scripts[hash], len=scripts[hash].length; i< len; i++ ) {
				$.getScript(array[i]);
			}
			$('li.active').toggleClass('active', false);
			$('[href="'+hash+'"]').parent('li').toggleClass('active', true);
		})
		.fail(function() {
			console.log("Error");
		});
	}

})();