<?php 

	class InventoryManager {
		private $db;
		public function __construct() {
			$this->db = connect('POS');
		}

		public function getCats() {
			$sql = "select distinct `itemType` from `inventory` order by `itemType`";

			$stat = $this->db->prepare($sql);
			$stat->execute();
			$rs = $stat->fetchAll();

			return $rs; 
		}

		public function getCategoryItems($category) {
			if(!isset($category)) return false;

			$sql = "select `itemID`, `itemType`, `itemName`, `itemDescription`, `itemPrice`, 
			`itemImage` from `inventory` where deleted = 0 and `itemType` = ?";

			$stat = $this->db->prepare($sql);
			$stat->bindParam('?', $category);
			$stat->execute();
			$allItem = $stat->fetchAll();

			return $allItem;
		}

		public function getAllItems() {
			$sql = "select `itemID`, `itemType`, `itemName`, `itemDescription`, 
			`itemPrice`, `itemImage` from `inventory` where deleted = 0";

			$stat = $this->db->prepare($sql);
			$stat->execute();
			$rs = $stat->fetchAll();

			return $rs; 
		}
	}

?>