CREATE DATABASE  IF NOT EXISTS `POS` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `POS`;
-- MySQL dump 10.13  Distrib 5.6.19, for linux-glibc2.5 (x86_64)
--
-- Host: 127.0.0.1    Database: POS
-- ------------------------------------------------------
-- Server version	5.5.41-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sales`
--

DROP TABLE IF EXISTS `sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales` (
  `idSales` int(11) NOT NULL AUTO_INCREMENT,
  `itemID` int(11) NOT NULL,
  `itemPrice` float DEFAULT '0',
  `itemName` varchar(45) DEFAULT NULL,
  `itemDescription` varchar(200) DEFAULT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `idTrans` int(11) NOT NULL,
  `dateSales` datetime DEFAULT NULL,
  PRIMARY KEY (`idSales`),
  UNIQUE KEY `idSales_UNIQUE` (`idSales`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales`
--

LOCK TABLES `sales` WRITE;
/*!40000 ALTER TABLE `sales` DISABLE KEYS */;
INSERT INTO `sales` VALUES (99,31,0.5,'Coke','Can of coke',1,43,NULL),(100,31,0.5,'Coke','Can of coke',1,44,NULL),(101,32,0.5,'Cookies','Cookies',1,44,NULL),(102,31,0.5,'Coke','Can of coke',2,45,NULL),(103,32,0.5,'Cookies','Cookies',1,45,NULL),(104,33,2,'Starbucks','Starbucks Frappucino',1,45,NULL),(105,31,0.5,'Coke','Can of coke',1,46,NULL),(106,32,0.5,'Cookies','Cookies',1,46,NULL),(107,33,2,'Starbucks','Starbucks Frappucino',1,46,NULL);
/*!40000 ALTER TABLE `sales` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-02-17 15:23:25
