function manifest() {
	this.rawData = [];
	this.ui = {};

	this.init();
}

manifest.prototype = {
	constructor: manifest,

	init: function() {
		this.fetchData();
	},

	fetchData: function() {
		var url = "library/php/inventory.php";
		var obj = this;

		$.ajax({
			type: 'POST',
			async: false,
			url: url,
			success: function(oJson) {
				obj.rawData = oJson;
			}
		});
	},

	assignUI: function(container) {
		this.ui = new uiManifest(this);
		this.ui.container = container;
		this.ui.printHtml();
	},

	updateDatabase: function(json) {
		var obj = this;
		var rs = false;
		$.ajax({
			url: "InventoryManagement/php/dbInventoryItemActions.php",
			data: json,
			type: 'POST',
			async: false,
			cache: false
		}).done(function(result) {
			if (result.trim() == "success") {
				obj.ui.table.ajaxResult.html('<span class="label label-success">Saved</span>');
				setTimeout(function() {obj.ui.table.ajaxResult.html('')}, 3000);
				obj.fetchData();
				rs = true;
			} else {
				obj.ui.table.ajaxResult.html('<span class="label label-danger">' + result + '</span');
				setTimeout(function() {obj.ui.table.ajaxResult.html('')}, 3000);
				rs = false;
			}
		});

		return rs;
	}
}

function uiManifest(manifest) {
	this.manifest = manifest;
	this.table = {};
	this.container = {};
	this.ajaxResult = {};
}

uiManifest.prototype = {
	constructor: uiManifest,

	printHtml: function() {
		this.table = new DataTable("tblManifest", this.manifest.rawData, this.container);
		this.table.printHtml();
		this.addons();
		this.bindUIEvent();
		this.bindAutoSuggestCategories();
	},

	addons: function() {
		var obj = this;

		// append upload image button for images
		var btnUpload = '<button class="btnUploadItemImg btn btn-default btn-sm" data-toggle="modal" data-target="#modalPopup">';
		btnUpload += '<span class="glyphicon glyphicon-cloud-upload"></span>';
		btnUpload += '</button>';
		obj.container.find("td.itemImage").append(btnUpload);
		obj.bindImageUpload();
	},

	bindImageUpload: function() {
		$(".btnUploadItemImg").on("click", function() {
			$('.btnUploadItemImg.active').toggleClass('active', false);
			$(this).toggleClass('active', true);

			$.ajax({
				url: 'InventoryManagement/FileUpload.html'
			}).done(function(result) {
				$("body").find('.modal').remove();
				$("body").append(result);
			});
		});
	},

	bindAutoSuggestCategories: function() {
		var obj = this;
		var categories;

		$.ajax({
			url: 'InventoryManagement/php/categories_json.php',
			async: false
		}).done(function(json) {
			categories = json;
			obj.container.find('input[name="itemType"]').autocomplete({
				source: categories,
				autoFocus: true
			});
		});
	},

	bindUIEvent: function() {
		var obj = this;

		this.container.find(".addRecord").bind({
			click: function() {
				var jsonData = { action: "AddRecord" };
				var row = $(this).closest('tr');
				row.find(".newRecord").each(function() {
					jsonData[$(this).attr("name")] = $(this).val();
				});

				var success = obj.manifest.updateDatabase(jsonData);
				if (success) { obj.printHtml(); }
			}
		});

		this.container.find(".removeRecord").bind({
			click: function() {
				var jsonData = { action: "DeleteRecord" };
				var row = $(this).closest('tr');
				row.find(".currentRecord").each(function() {
					jsonData[$(this).attr("name")] = $(this).val();
				});

				var success = obj.manifest.updateDatabase(jsonData);
				
				// If was delete allow 4 seconds to undo action
				if (success) { 
					$(this).switchClass("DeleteRecord", "UndoDelete"); //switchClass jquery ui method
					$(this).switchClass("btn-warning", "btn-danger");

					var deleted = true;
					$(this).html("Undo");

					row.fadeOut(7000, function() {
						if (deleted) { row.remove() };
					});

					$(".UndoDelete").one("click", function() { 
						jsonData.action = "UndoDelete";
						success = obj.manifest.updateDatabase(jsonData);

						if (success) {
							row.stop(true);
							row.fadeIn(1000);
							deleted = false;
							$(this).html("Remove");
							$(this).switchClass("UndoDelete", "DeleteRecord");
							$(this).switchClass("btn-danger", "btn-warning");
						}
					});

				}
			}
		});

		this.container.find(".currentRecord").bind({
			// change event to identify the row with changed values
			change: function() {
				var row = $(this).closest('tr');
				if (!row.hasClass("changedRecord")) {
					row.toggleClass("changedRecord", true);
				}
			}, 

			keyup: function() {
				obj.table.tableFoot.find(".inputMirror").val($(".focused").val());
			},

			focus: function() {
				$(".focused").toggleClass("focused", false);
				$(this).toggleClass("focused", true);
				obj.table.tableFoot.find(".inputMirror").val($(".focused").val());
			},

			// blur event update records
			blur: function() {
				var row = $(this).closest('tr');

				if (row.hasClass("changedRecord")) {
					var jsonData = { action: "UpdateRecord" };

					row.find(".currentRecord").each(function() {
						jsonData[$(this).attr("name")] = $(this).val();
					});

					obj.manifest.updateDatabase(jsonData);
					row.toggleClass("changedRecord", false);
				}
			}

		});

		this.table.tableFoot.find(".inputMirror").bind({
			keyup: function() {
				$(".focused").val($(this).val());
				var row = $(".focused").closest('tr');

				if (!row.hasClass("changedRecord")) {
					row.toggleClass("changedRecord", true);
				}
			},

			blur: function() {
				var row = $(".changedRecord").closest('tr');

				if (row.hasClass("changedRecord")) {
					var jsonData = { action: "UpdateRecord" };

					row.find(".currentRecord").each(function() {
						jsonData[$(this).attr("name")] = $(this).val();
					});

					obj.manifest.updateDatabase(jsonData);
					row.toggleClass("changedRecord", false);
				}
			}
		});
	}
}