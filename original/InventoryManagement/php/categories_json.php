<?php header('Content-Type: application/json');
include "../../library/php/mySqlConnection.php";

ini_set("display_error", "on");
error_reporting(E_ALL | E_STRICT);

$cn = new sqlConnection;
$db = $cn->connect("POS");

$categories = [];
$json = [];

$sql = "SELECT DISTINCT `itemType` FROM `inventory` ORDER BY `itemType` ";

$rs = $db->query($sql);

while ($row = $rs->fetch_assoc()) {
	array_push($categories, $row['itemType']);
}

echo json_encode($categories);

$db->close();
?>