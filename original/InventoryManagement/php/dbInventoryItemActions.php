<?php
include "../../library/php/mySqlConnection.php";

//ini_set("display_error", "on");
//error_reporting(E_ALL | E_STRICT);

$cn = new sqlConnection;
$db = $cn->connect("POS");

$action = (isset($_POST['action']) ? trim($_POST["action"]) : null);
$itemID = (isset($_POST['itemID']) ? intval($_POST["itemID"]) : null);
$itemName = (isset($_POST["itemName"]) ? trim($_POST["itemName"]) : null);
$itemPrice = (isset($_POST["itemPrice"]) ? floatval($_POST["itemPrice"]) : null);
$itemType = (isset($_POST["itemType"]) ? trim($_POST["itemType"]) : null);
$itemDescription = (isset($_POST["itemDescription"]) ? trim($_POST["itemDescription"]) : null);
$itemImage = (isset($_POST["itemImage"]) ? trim($_POST["itemImage"]) : null);

switch ($action) {
	case "AddRecord":
		$sql = 	"INSERT INTO inventory 
		(itemName, itemPrice, itemType, itemDescription, itemImage)
		VALUES (?, ?, ?, ?, ?)";

		if ($stmt = $db->prepare($sql)) 
		{
			$stmt->bind_param("sdsss", $itemName, $itemPrice, $itemType, $itemDescription, $itemImage);

			$stmt->execute();

			if (mysqli_affected_rows($db) > 0) {
				echo "success";	
			} else {
				echo "Failed to add new record.";
			}

			$stmt->close();
		}

		break;

	case "UpdateRecord":
		$sql = 	"UPDATE inventory SET itemName = ?, itemPrice = ?, itemType = ?,
		itemDescription = ?, itemImage = ? WHERE itemID = ?";

		if ($stmt = $db->prepare($sql)) 
		{
			$stmt->bind_param("sdsssi", 
				$itemName, 
				$itemPrice, 
				$itemType, 
				$itemDescription, 
				$itemImage, 
				$itemID);

			$stmt->execute();
			//echo mysqli_affected_rows($db);
			if (mysqli_affected_rows($db) > 0) {
				echo "success";	
			} else {
				echo "Failed to update record.";
			}

			$stmt->close();
		}

		break;

	case "DeleteRecord":
		$sql = 	"UPDATE inventory SET deleted = 1 WHERE itemID = ? ";

		if ($stmt = $db->prepare($sql)) 
		{
			$stmt->bind_param("i", $itemID);

			$stmt->execute();
			//echo mysqli_affected_rows($db);
			if (mysqli_affected_rows($db) > 0) {
				echo "success";	
			} else {
				echo "Failed to update record.";
			}

			$stmt->close();
		}

		break;

	case "UndoDelete":
		$sql = 	"UPDATE inventory SET deleted = 0 WHERE itemID = ? ";

		if ($stmt = $db->prepare($sql)) 
		{
			$stmt->bind_param("i", $itemID);

			$stmt->execute();
			//echo mysqli_affected_rows($db);
			if (mysqli_affected_rows($db) > 0) {
				echo "success";	
			} else {
				echo "Failed to update record.";
			}

			$stmt->close();
		}

		break;
	default:
		echo "Unknown Action Requested. No Changes Made.";
}	

$db->close();
?>
