function POSModule() {
	this.pageComponents = ["POS.html"];
	this.container = $("#page-wrapper");
	this.statusDisplay = $("#statusDisplay");

	this.init();
}

POSModule.prototype = {
	constructor: POSModule,

	init: function() {
		this.loadPageComponents();
		this.bindUIEvent();
	},

	bindUIEvent: function() {

	},

	loadPageComponents: function() {
		var obj = this;

		for (var i = 0; i < this.pageComponents.length; i++)
		{
			var url = this.pageComponents[i];
			obj.statusDisplay.html('<img src="../images/loading.gif" alt="loading" />');
			$.ajax({
				url: url,
				async: false,
				success: function(oHtml) {
					obj.container.append(oHtml);
					obj.statusDisplay.html('');
				}
			});
		}
		
	}
}