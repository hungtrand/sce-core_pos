function POSModule() {
	this.page = "POS/shelf.html";
	this.container = $("#page-wrapper");

	this.init();
}

POSModule.prototype = {
	constructor: POSModule,

	init: function() {
		this.bindUIEvent();
		this.loadPage();
	},

	bindUIEvent: function() {

	},

	loadPage: function() {
		var obj = this;
		var data = {

		};

		var url = this.page;

		obj.container.html('<img src="library/images/loading.gif" alt="loading" />');
		$.ajax({
			data: data,
			url: url
			success: function(oHtml) {
				obj.container.html(oHtml);
			}
		});
	}
}