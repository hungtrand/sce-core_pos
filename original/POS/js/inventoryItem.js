function inventoryItem(data) {
	this.ID = data.ID;
	this.Name = data.Name;
	this.Type = data.Type;
	this.Description = data.Description;
	this.Price = data.Price;
	this.ImageSrc = data.ImageSrc;
	this.ui;

	this.init();
}

inventoryItem.prototype = {
	constructor: inventoryItem,

	init: function() {
		this.ui = new uiInventoryItem(this);
	},

	updateName: function(newName) {

	},

	updateDescription: function(newDescription) {

	},

	updatePrice: function(newPrice) {

	},

	updateImageSrc: function(newImg) {

	}, 

	assignUI: function(container) {
		this.ui.container = container;
		this.ui.printHtml();
	}
}

function uiInventoryItem(item) {
	this.container = {};
	this.item = item;
	this.element;

	this.init();
}

uiInventoryItem.prototype = {
	constructor: uiInventoryItem,

	init: function() {
		this.loadHtml();
	},

	loadHtml: function() {
		var oHtml = "";
		var obj = this;
		oHtml = $($('#InventoryItemTemplate').html());
		oHtml.find("#InventoryItem--").attr("id", "InventoryItem" + obj.item.ID);
		oHtml.find("input[name='itemID']").val(obj.item.ID);
		oHtml.find("input[name='itemType']").val(obj.item.Type);
		oHtml.find("input[name='itemType']").val(0);

		var imgUrl = "";
		if (FileExists("library/images/products/" + obj.item.ImageSrc))
		{
			imgUrl = "library/images/products/" + obj.item.ImageSrc;
		} else {
			imgUrl = "library/images/" + "Product-icon.png";
		}

		oHtml.find(".itemImage").find("img").attr("src", imgUrl);
		oHtml.find(".itemImage").find("img").attr("title", obj.item.Name);
		oHtml.find(".itemLabel").find(".itemName").text(obj.item.Name);
		oHtml.find(".itemLabel").find(".itemPrice").text('$ ' + obj.item.Price);

		obj.element = oHtml;
	},

	printHtml: function() {
		this.container.append(this.element); 
		this.bindUIEvent();
	},

	addToCart: function() {
		var that = this;

		var qty = parseInt(that.element.find('.qtyInCart').val()); 
		if (isNaN(qty)) return;

		that.element.find('.qtyInCart').val(qty++);
	},

	removeFromCart: function() {
		var that = this;

		var qty = parseInt(that.element.find('.qtyInCart').val()); 
		if (isNaN(qty) || qty == 0) return;

		that.element.find('.qtyInCart').val(qty--);
	},

	bindUIEvent: function() {
		var obj = this;

		obj.element.bind({
			mouseenter: function() {$(this).toggleClass("mouseover-outset")},
			mouseleave: function() {$(this).toggleClass("mouseover-outset")},
			click: function() {
				obj.addToCart();
				obj.refresh();
			}
		});

		obj.element.find(".numInCart").bind({
			mouseenter: function() {
				if ($(this).text().length > 0) {
					$(this).attr("tempQty", $(this).text());
					$(this).text("X");
					$(this).toggleClass('btnRemoveFromCart', true);
				}
			},
			mouseleave: function() {
				if ($(this).attr("tempQty")) {
					$(this).text($(this).attr("tempQty"));
					$(this).toggleClass('btnRemoveFromCart', false);
				}
			},
			click: function() {
				event.stopPropagation();
				obj.removeFromCart();
				obj.refresh();
			}
		});
	},

	refresh: function() {
		var qty = this.item.cart.cartItemsQty[this.item.ID];
		var uiQtyInCart = this.element.find(".numInCart");

		if (qty != 0 && typeof(qty) == 'number') {
			uiQtyInCart.text(qty);
			uiQtyInCart.css("visibility", "visible");
		} else {
			uiQtyInCart.text(0);
			uiQtyInCart.css("visibility", "hidden");
		}
	}
	
}
