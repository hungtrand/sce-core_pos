function shelf() {
	this.inventoryCount = 0;
	this.inventoryData = [];
	this.inventoryItems = {};
	this.ui;
	this.checkout = new transaction();

	this.init();
}

shelf.prototype = {
	contructor: shelf,

	init: function(container) { 
		var that = this;
		this.loadInventoryData(function() { 
			that.loadShelf(); 
			that.ui = new uiShelf(that);
			that.assignUI(container);
			that.ui.printHtml();
		});
		
	},

	// get available inventoryItem data from database in json form
	loadInventoryData: function(callback) {
		var url = "library/php/inventory.php";
		var obj = this;

		$.ajax({
			type: 'POST',
			url: url,
			success: function(oJson) {
				obj.inventoryData = oJson;
				callback();
			}
		});
	},

	// fill shelf's array with inventoryItem objects
	loadShelf: function() {
		var arrData = this.inventoryData;
		// Iterate through the inventoryItems array
		for (i = 0; i < arrData.length; i++) {
			// Clean data
			var itemData = {
				ID: parseInt(arrData[i].itemID),
				Name: (arrData[i].itemName + "").trim(),
				Type : (arrData[i].itemType + "").trim(),
				Description: (arrData[i].itemDescription + "").trim(),
				Price: parseFloat(arrData[i].itemPrice).toFixed(2),
				ImageSrc: (arrData[i].itemImage + "").trim()
			}

			this.addShelfItem(itemData);	
		}
	},

	// Helper: Construct inventoryItem objects
	addShelfItem: function(itemData) { 
		// create as associative array for convenient retrieval
		this.inventoryItems[itemData.ID] = new inventoryItem(itemData);
	},
	
	assignUI: function(container) {
		this.ui.container = container;
	}
}

function uiShelf(objShelf) {
	this.shelf = objShelf;
	this.container = {};

	this.init();
}

uiShelf.prototype = {
	constructor: uiShelf,

	init: function() {
		
	},

	printHtml: function() {
		var obj = this;
		var dict = this.shelf.inventoryItems;

		for (item in dict) {
			dict[item].assignUI(this.container);
			dict[item].shelf = this.shelf;
		}
	},

	bindUIEvent: function() {

	}
}