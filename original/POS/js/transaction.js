function transaction() {
	this.cart = {};
	this.total = 0.00;
	this.paid = 0.00;
	this.due = 0.00;
	this.lastTransaction = {};

	this.ui = {};

	this.init();
}

transaction.prototype = {
	constructor: transaction,

	init: function() {
		this.ui = new uiTransaction(this);
	},

	assignCart: function(cart) {
		this.cart = cart;
	},

	assignUI: function(container) {
		this.ui.container = container;
		this.ui.printForm();
	},

	calculate: function () {
		// Calculate total
		var newTotal = 0.0;
		for (itemID in this.cart.cartItems)
		{
			newTotal += this.cart.cartItems[itemID].Price * this.cart.cartItemsQty[itemID];
		}
		
		// get amount paid and due
		this.total = parseFloat(newTotal).toFixed(2);

		if (parseFloat(this.ui.paid.val()) != parseFloat(this.ui.total.val()) && this.total != 0) {
			this.paid = parseFloat(this.ui.paid.val()).toFixed(2);
		} else {
			this.paid = this.total;
		}
		
		this.due = (this.total - this.paid).toFixed(2);

		// update interface
		this.ui.refresh();
	},

	checkout: function () {
		var obj = this;
		var data = {};
		var sales = {};

		for (itemID in this.cart.cartItems) {
			sales[itemID] = {};
			sales[itemID].ID = this.cart.cartItems[itemID].ID;
			sales[itemID].Name = this.cart.cartItems[itemID].Name;
			sales[itemID].Description = this.cart.cartItems[itemID].Description;
			sales[itemID].Price = this.cart.cartItems[itemID].Price;
			sales[itemID].Quantity = this.cart.cartItemsQty[itemID];
		}

		data['SJSUID'] = obj.ui.SJSUID.val();
		data['sales'] = sales;

		var url = "POS/php/checkout.php";
		
		$.ajax({
			data: JSON.stringify(data),
			url: url,
			async: false,
			cache: false,
			type: 'POST',
			contentType: 'application/json; charset=UTF-8'
		}).done(function(json) {
			obj.lastTransaction = json;
			obj.reset();
			obj.ui.refresh();
			obj.ui.updateLastTransaction();
		});
	},

	reset: function() {
		this.cart.resetCart();
		this.calculate();
		this.ui.SJSUID.val('');
	}
}

function uiTransaction(transaction) {
	this.container = {};
	this.transaction = transaction;

	this.theForm;
	this.total;
	this.paid;
	this.due;
	this.SJSUID;
	this.checkout;
	this.reset;
	this.lastTransaction;
}

uiTransaction.prototype = {
	constructor: uiTransaction,

	refresh: function() {
		this.total.val(this.transaction.total);
		this.paid.val(this.transaction.paid);
		this.due.val(this.transaction.due);
	},

	bindUIEvent: function() {
		var obj = this;

		this.total.on("click", function() {
			$(this).select();
		});

		this.paid.bind({
			click: function() {
				$(this).select();
			},
			change: function() {
				obj.transaction.calculate();
			}
		});

		this.checkout.bind({
			click: function() {
				var msg = "Confirmation\n\nTotal: " + obj.total.val() + "\n\nCheck out?"; 
				var conf = confirm(msg);

				if (conf)
					obj.transaction.checkout();
				else
					return false;
			}
		});

		this.reset.bind({
			click: function() {
				obj.transaction.reset();

				return false;
			}
		});
	},

	printForm: function() {
		var obj = this;
		var url = "POS/transaction.html";

		$.ajax({
			url: url,
			type: 'POST',
			async: false,
			success: function(oHtml) {
				obj.container.html(oHtml);
				obj.total = obj.container.find("input[name='total']");
				obj.paid = obj.container.find("input[name='paid']");
				obj.due = obj.container.find("input[name='due']");
				obj.SJSUID = obj.container.find("input[name='SJSUID']");
				obj.checkout = obj.container.find("button.checkout");
				obj.reset = obj.container.find("button.reset");
				obj.theForm = obj.container.find("form");
				obj.lastTransaction = obj.container.find(".lastTransaction");
				obj.bindUIEvent();
			}
		});
	},

	updateLastTransaction: function() {
		var obj = this;
		var jsonLast = obj.transaction.lastTransaction;

		var str = "[ID: " + jsonLast.idTrans + "] ";
		str += "[Total: $" + parseFloat(jsonLast.totalTrans).toFixed(2) + "]";
		obj.lastTransaction.html(str);
	}
}