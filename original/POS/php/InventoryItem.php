<?php
	$itemID = $_POST["itemID"];
	$itemName = $_POST["itemName"];
	$itemType = $_POST["itemType"];
	$itemDescription = $_POST["itemDescription"];
	$itemPrice	= $_POST["itemPrice"];
	$itemImage = $_POST["itemImage"];

	if (empty($itemImage) > 0) {
		$itemImage = "library/images/Product-icon.png";
	}
?>

<div class="divInventoryItem" id="inventoryItem<?php echo($itemID); ?>">
	<!-- Hidden Inputs -->
	<input type="hidden" class="itemID" name="itemID" value="<?php echo($itemID); ?>" />
	<input type="hidden" class="itemType" name="itemType" value="?<?php echo($itemType); ?>" />
	<div class="numInCart">0</div>

	<div class="itemImage">
		<img src="<?php echo $itemImage ?>" alt="Product" title="<?php echo($itemDescription); ?>" />
	</div>

	<div class="itemLabel">
		<span class="itemName"><?php echo($itemName); ?></span>
		<span class="itemPrice">$<?php echo($itemPrice); ?></span>
	</div>

</div>