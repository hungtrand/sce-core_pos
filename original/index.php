<?php
    session_start();
    if (!isset($_SESSION['_ADMIN_'])) { 
        header('Location: ../../');
        session_write_close();
    }

    session_write_close();
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>POS - SCE-CORE Services Administration</title>

    <!-- Bootstrap Core CSS -->
    <link href="library/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="library/bootstrap/templates/grayscale/css/grayscale.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="library/bootstrap/templates/grayscale/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="css/index.css">
    <link rel="icon" type="image/x-icon" href="library/images/favicon.ico">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">
                    <img style="width: 32px;" src="library/images/SCE.png" title="SCE Core" /><span class="light">SCE</span> CORE
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#POS">POS</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#InventoryManagement">Inventory Management</a>
                    </li>
                     <li>
                        <a class="page-scroll" href="#Reports">Reports</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- About Section -->
    <section id="index-contents" class="container-fluid content-section text-center">

    </section>

    <!-- Footer -->
    <footer>
    </footer>

    <!-- jQuery Version 1.11.0 -->
    <script src="library/jquery/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="library/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="library/bootstrap/templates/grayscale/js/jquery.easing.min.js"></script>

    <!-- Custom Javascript -->
    <script src="library/js/utils.js"></script>
    <script src="js/index.js"></script>

</body>

</html>
