function DataTable(tableID, data, container) {
	this.tableHead = {};
	this.tableBody = {};
	this.tableFoot = {};
	this.tableID = tableID;
	this.tableData = data;
	this.container = container;
	this.frame;
	this.ajaxResult = {};

	this.init();
}

DataTable.prototype = {
	constructor: DataTable,

	init: function() {
		this.loadFrame();
	},

	loadFrame: function() {
		var url = "library/DataTable/html/dbTable.html";
		var obj = this;
		var oHtml = '';

		$.ajax({
			url: url,
			async: false,
			type: 'POST'
		}).done(function(result) {
			oHtml = $(result);
			obj.frame = oHtml.find(".tablesContainer");
			obj.container.html(obj.frame);
			obj.tableHead = obj.frame.find(".divTableHeadContainer").first();
			obj.tableHead.addClass(obj.tableID);
			obj.tableBody = obj.frame.find(".divTableBodyContainer").first();
			obj.tableBody.addClass(obj.tableID);
			obj.tableFoot = obj.frame.find(".divTableFootContainer").first();
			obj.ajaxResult = obj.frame.find(".ajaxResult");
		});
	},

	printTableHead: function() {
		var obj = this;
		var data = this.tableData;
		var cols = [];
		var addCols = [];
		var thead = obj.tableHead.find("thead").first();
		var addRecord = obj.tableHead.find("tbody").first();

		var head = document.createElement("tr");
		var addRow = document.createElement("tr");

		var j = 0;
		for (col in data[0]) {

			cols[j] = document.createElement("th");

			if (j != 0) {
				var columnName = document.createTextNode(makeLabel(col));
				cols[j].setAttribute("class", col);
			} else {
				var columnName = document.createTextNode("");
				cols[j].setAttribute("class", "firstColumn " + col);
			}

			// Creating the header with column names
			var label = document.createElement("label")
			label.appendChild(columnName);

			cols[j].appendChild(label);

			head.appendChild(cols[j]);

			if (j == 0) {
				var addRecordButton = document.createElement("button");
				addRecordButton.setAttribute("class", "addRecord btn btn-xs btn-success");
				addRecordButton.appendChild(document.createTextNode("Add"));

				addCols[j] = document.createElement("td");
				addCols[j].setAttribute("class", "firstColumn " + col);
				addCols[j].appendChild(addRecordButton);
			} else {
				// Creating empty row to add new records
				addCols[j] = document.createElement("td");
				addCols[j].setAttribute("class", col);
				var inputAdd = document.createElement("input");
				inputAdd.setAttribute("type", "text");
				inputAdd.setAttribute("name", col);
				inputAdd.setAttribute("class", "newRecord");

				addCols[j].appendChild(inputAdd);
			}

			addRow.appendChild(addCols[j]);

			j++;
		}

		thead.append(head);
		addRecord.append(addRow);
	},

	printTableBody: function() {
		var obj = this;
		var data = this.tableData;
		var cols = [];
		var tbody = obj.tableBody.find("tbody").first();

		for (var i = 0; i < data.length; i++) {
			var rowData = data[i];
			var row = document.createElement("tr");

			var j = 0;
			for (col in rowData) {
				// First column will be the buttons
				if (j == 0) {
					var removeRecordButton = document.createElement("button");
					removeRecordButton.setAttribute("class", "removeRecord btn btn-xs btn-warning");
					removeRecordButton.setAttribute("value", rowData[col]);
					removeRecordButton.appendChild(document.createTextNode("Remove"));
					
					var hiddenInput = document.createElement("input");
					hiddenInput.setAttribute("type", "hidden");
					hiddenInput.setAttribute("name", col);
					hiddenInput.setAttribute("class", "currentRecord");
					hiddenInput.setAttribute("value", rowData[col]);

					var cell = document.createElement("td");
					cell.setAttribute("class", "firstColumn " + col);
					cell.appendChild(removeRecordButton);
					cell.appendChild(hiddenInput);
					row.appendChild(cell);
					
				} else {
					// Build each row and append to the body
					var cell = document.createElement("td");
					cell.setAttribute("class", col);

					var input = document.createElement("input");
					input.setAttribute("type", "text");
					input.setAttribute("name", col);
					input.setAttribute("class", "currentRecord");
					input.value = rowData[col];

					cell.appendChild(input);
					row.appendChild(cell);
				}

				j++;
			}

			tbody.append(row);
		}
	},

	printTableFoot: function() {
		//this.container.append(this.tableFoot);
	},

	printHtml: function() {
		this.printTableHead();
		this.printTableBody();
		this.printTableFoot();
	}
}