function ReportTable(data) {
	this.tableData = data;
	this.container;
	this.contents;

	this.init();
}

ReportTable.prototype = {
	constructor: ReportTable,

	init: function() {
		this.loadFrame();
	},

	loadFrame: function() {
		var url = "library/ReportTable/html/frame.html";
		var obj = this;
		var oHtml = '';

		$.ajax({
			url: url,
			type: 'POST'
		}).done(function(result) {
			obj.contents = $(result);
		});
	},

	hideColumns: function(arrColumns) {

	},

	print: function(container) {
		this.container.html(this.contents);
	}
}