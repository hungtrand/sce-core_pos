function main(settings) {
	this.PageNavigation = settings.PageNavigation;
	this.PageComponents = settings.PageComponents;
	this.ContentContainer = settings.ContentContainer;
	this.StatusDisplay = settings.StatusDisplay;

	this.init();
}

main.prototype = {
	constructor: main,

	init: function() {
		this.loadNav();
	},

	loadNav: function() {
		var obj = this;
		var nav = this.PageNavigation;

		for (id in this.PageComponents) {
			var pc = this.PageComponents[id];
			var oHtml = '';
			
			$.ajax({
				url: "NavLink.html",
				async: false,
				type: 'POST',
				success: function(result) {
					result = result.trim();
					result = result.replace("#linkID#", pc.linkID);
					result = result.replace("#linkTitle#", pc.linkTitle);
					result = result.replace("#linkSrc#", "#" + pc.linkID);
					result = result.replace("#linkIcon#", pc.linkIcon);

					oHtml = $(result);

					nav.append(oHtml);
					obj.bindLinkEvent(pc.linkID);
				}
			});
		}
	},

	bindLinkEvent: function(id) {
		var obj = this;
		var link = $("#" + id);

		link.bind({
			click: function(event) {
				event.preventDefault();
				
				obj.loadPage(id);
			}
		});
	},

	loadPage: function(id) {
		var obj = this;
		var url = this.PageComponents[id].linkSrc;

		obj.StatusDisplay.html('<img class="imgLoading" src="images/loading.gif" alt="loading" />');
		$.ajax({
			url: url,
			cache: false,
			async: false,
			success: function(oHtml) {
				obj.ContentContainer.html(oHtml);
				obj.StatusDisplay.html('');
				window.location.hash = "#" + id;
				$("li.active").toggleClass("active", false);
				$("#" + id).closest("li").toggleClass("active", true);
			}
		});
	}
}