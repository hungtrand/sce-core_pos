// Convert any oneWordName to label One Word Name
function makeLabel(str) {
	str = str.trim();
	var label = "";

	for (var i = 0; i < str.length; i++) {
		if (i == 0) {
			label = str[i].toUpperCase();
		} else {
			if (str[i] == str[i].toUpperCase() && str[i-1] != str[i-1].toUpperCase()) {
				label += " ";
			}

			label += str[i];
		}
	}

	return label;
}

// Check if image or file exists through ajax
function FileExists(path) {
	var http = jQuery.ajax({
		type:"HEAD",
		url: path,
		async: false
	});
	
	if (http.status == 200) {
		return true;
	} else {
		return false;
	}
}