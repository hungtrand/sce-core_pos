<?php header('Content-Type: application/json');
include "mySqlConnection.php";
$cn = new sqlConnection;

$db = $cn->connect("POS");

$sql = "SELECT itemID, itemName, itemDescription,
		itemPrice, itemImage, itemType FROM inventory
		WHERE deleted != 1 ORDER BY itemType, itemName";

$rs = $db->query($sql);

$list = array();

while ($row = $rs->fetch_assoc())
{
	$arr = array("itemID" => $row['itemID'], 
		"itemName" => $row['itemName'],
		"itemType" => $row['itemType'],
		"itemDescription" => $row['itemDescription'], 
		"itemPrice" => $row['itemPrice'], 
		"itemImage" => $row['itemImage']);

	array_push($list, $arr);
}

echo json_encode($list);

?>